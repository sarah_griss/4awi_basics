package at.sarah.griss.remote;

public class Remote {
	private boolean isOn = false;
	private boolean hasPower = true;
	private Battery battery;
	private boolean colorOn = false;

	public Remote(Battery battery) {
		super();
		this.battery = battery;
	}

	public void turnOn() {
		this.isOn = true;
		System.out.println("Remote is on now!");
	}
	
	public void turnOff() {
		this.isOn = false;
		System.out.println("Remote is of now!");
	}
	
	public boolean getStatus() {
		return this.isOn;
	}

	public void colorOn() {
		this.colorOn = true;
		System.out.println("The Beamer is now on color Mode!");
	}
	
	public void colorOff() {
		this.colorOn = false;
		System.out.println("The Beamer is now on Black and White Mode!");
	}
	

	

}
