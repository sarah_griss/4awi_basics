package at.sarah.griss.remote;

public class BeamerStarter {

	public static void main(String[] args) {
		
		Battery b1 = new Battery();
		Remote r1 = new Remote(b1);
		
		Battery b2 = new Battery();
		Remote r2 = new Remote(b2);
		
		r1.turnOn();
		System.out.println(r1.getStatus());
		
		System.out.println(b1.getChargeStatus());
		b1.setChargeStatus(20);
		System.out.println(b1.getChargeStatus());
		
		System.out.println(b1.Batterystatus());
		
		
		r2.turnOn();
		System.out.println(r2.getStatus());
		
		System.out.println(b2.getChargeStatus());
		b2.setChargeStatus(90);
		System.out.println(b2.getChargeStatus());
		
		System.out.println(b2.Batterystatus());

		System.out.println(r1.getStatus());
		
		r1.colorOn();
		r2.colorOff();
	}

}
