package at.sarah.griss.remote;

public class Battery {
	private int chargeStatus = 100;

	public Battery() {
		super();
	}

	public int getChargeStatus() {
		return chargeStatus;
	}

	public void setChargeStatus(int chargeStatus) {
		this.chargeStatus = chargeStatus;
	}
	
	public boolean Batterystatus() {
		
		if(chargeStatus > 40) {
			
			System.out.println("Battery has " + chargeStatus +"%. Battery is good.");
			return true;
		}else {
			
			System.out.println("Battery has " + chargeStatus +"%. Battery is soon empty.");
			return false;
		}
	}

}
