package at.sarah.griss.ractangle;

public class Rectangle {
	// Gedächtnisvariable- Instanz- Objektvariabe
	private int a, b;
	private double areaResult;

	public Rectangle(int a, int b) {
		this.a = a;
		this.b = b;
	}

	// a und b zurückliefern
	public int getA() {
		return this.a;
	}

	public int getB() {
		return this.b;
	}

	// a und b ändern
	public void setA(int a) {
		this.a = a;
	}

	public void setB() {
		this.b = b;
	}

	// Länge und Breite des Rectangles ausgeben
	public void sayHello() {
		System.out.println("Ich bin " + this.a + " breit und " + this.b + " lang!");
	}

	// GetArea
	public double getArea() {
		areaResult = (this.a * this.b);
		return areaResult;
	}

	// getCircumference
	public double getCircumference() {
		return (this.a * 2) + (this.b * 2);
	}
}
