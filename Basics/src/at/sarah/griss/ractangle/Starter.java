package at.sarah.griss.ractangle;
public class Starter {

	public static void main(String[] args) {
		Rectangle r1 = new Rectangle (4,2);
		Rectangle r2 = new Rectangle (7,3);
		Rectangle r3 = r2;
		
		r1.sayHello();
		r2.sayHello();
		r3.sayHello();
		
		System.out.println("Flächeninhalt " + r1.getArea());
		System.out.println("Seite A " + r1.getA());
		System.out.println("Seite B " + r1.getB());
		System.out.println("Umfang " + r1.getCircumference());
	}

}
