package at.sarah.griss.car;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Person {

	private String firtsName;
	private String lastName;
	private LocalDate today = LocalDate.now(); // Today's date
	private LocalDate birthDay; // Birth date

	private List<Car> cars;

	public Person(String firstName, String lastName, LocalDate birthDay) {
		this.firtsName = firstName;
		this.lastName = lastName;
		this.birthDay = birthDay;

		this.cars = new ArrayList<>();

	}

	public String getFirtsName() {
		return firtsName;
	}

	public String getLastName() {
		return lastName;
	}

	// Add cars to the person
	public void addCar(Car car) {
		this.cars.add(car);
	}

	// Get Age of Person
	public int getAge() {

		Period p = Period.between(birthDay, today);
		System.out.println(this.getFirtsName() + " " + this.getLastName() + " has the age: " + p.getYears());

		return p.getYears();
	}

	// Get Price of all Cars in the Arraylist
	public double getValueOfCars() {

		double price = 0;
		int count = 1;
		for (Car car : cars) {
			price += car.getPrice();
			System.out.println("Price of Car " + count++ + ": " + price);
		}

		System.out.println("Price of all Cars from " + this.getFirtsName() + " " + this.getLastName() + ": " + price);
		return price;

	}

}
