package at.sarah.griss.car;

import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

public class StarterCar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Engine e1 = new Engine("Benzin", 120);
		Engine e2 = new Engine("Diesel", 240);
		Manufacturer m1 = new Manufacturer("BMW", "DE", 7.5);

		Car c1 = new Car("Black", 15000, 120, 7, 35000, 7, m1, e1);
		Car c2 = new Car("Black", 12000, 120, 7, 60000, 7, m1, e1);
		Car c3 = new Car("Red", 90000, 230, 7, 10000, 7, m1, e2);
		Car c4 = new Car("Yellow", 56000, 210, 7, 65000, 7, m1, e2);

		Person p1 = new Person("Hans", "Peter", LocalDate.of(1980, Month.JULY, 16));
		p1.addCar(c1);
		p1.addCar(c2);

		Person p2 = new Person("Anna", "Nachbauer", LocalDate.of(1998, Month.DECEMBER, 29));
		p2.addCar(c3);
		p2.addCar(c4);

		System.out.println(p1.getAge());
		System.out.println(p1.getValueOfCars());

		System.out.println(p2.getAge());
		System.out.println(p2.getValueOfCars());

		// System.out.println(c1.getUsage());
		// System.out.println(c2.getUsage());

		// System.out.println(c1.getType());
		// System.out.println(c2.getType());

		// System.out.println(c1.getPrice());
		// System.out.println(c2.getPrice());

	}

}
