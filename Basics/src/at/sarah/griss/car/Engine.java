package at.sarah.griss.car;

public class Engine {
	private String fuelType;
	private float power;

	public Engine(String fuelType, float power) {
		super();
		this.fuelType = fuelType;
		this.power = power;
	}

	public String getFuelType() {
		return fuelType;
	}

	public float getPower() {
		return power;
	}

}
