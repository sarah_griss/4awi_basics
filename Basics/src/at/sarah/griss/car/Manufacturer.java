package at.sarah.griss.car;

public class Manufacturer {
	private String nameManufacturer;
	private String countryManufacturer;
	private double discountManufcaturer;

	public Manufacturer(String nameManufacturer, String countryManufacturer, double discountManufcaturer) {
		super();
		this.nameManufacturer = nameManufacturer;
		this.countryManufacturer = countryManufacturer;
		this.discountManufcaturer = discountManufcaturer;
	}

	public String getNameManufacturer() {
		return nameManufacturer;
	}

	public String getCountryManufacturer() {
		return countryManufacturer;
	}

	public double getDiscountManufcaturer() {
		return discountManufcaturer;
	}

}
