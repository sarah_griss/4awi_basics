package at.sarah.griss.car;

public class Car {
	private String color;
	private float basicPrice;
	private float topSpeed;
	private double fuelConsumption;
	private float kM;
	private double basicUsage;
	private String serialNumber;

	private Engine engine;
	private Manufacturer manufacturer;

	public Car(String color, float basicPrice, float topSpeed, double fuelConsumption, float kM, double basicUsage,
			Manufacturer manufacturer, Engine engine) {
		super();
		this.color = color;
		this.basicPrice = basicPrice;
		this.topSpeed = topSpeed;
		this.fuelConsumption = fuelConsumption;
		this.kM = kM;
		this.basicUsage = basicUsage;
		this.engine = engine;
		this.manufacturer = manufacturer;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public float getBasicPrice() {
		return basicPrice;
	}

	public void setBasicPrice(float basicPrice) {
		this.basicPrice = basicPrice;
	}

	public float getTopSpeed() {
		return topSpeed;
	}

	public void setTopSpeed(float topSpeed) {
		this.topSpeed = topSpeed;
	}

	public double getFuelConsumption() {
		return fuelConsumption;
	}

	public void setFuelConsumption(double fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public float getkM() {
		return kM;
	}

	public void setkM(float kM) {
		this.kM = kM;
	}

	public Engine getEngine() {
		return engine;
	}

	public Manufacturer getManufacturer() {
		return manufacturer;
	}

	// Fuel Consumption of car
	public double getUsage() {

		if (this.kM < 50000) {

			this.fuelConsumption = this.basicUsage;

		} else {

			this.fuelConsumption = this.basicUsage * 1.098;
		}

		return fuelConsumption;
	}

	// Price of the car with manufacturer discount
	public double getPrice() {
		return this.basicPrice * (100 - manufacturer.getDiscountManufcaturer()) / 100;

	}

	// Fuel Type of the car
	public String getType() {
		return engine.getFuelType();
	}

}
