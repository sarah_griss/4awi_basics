package at.gri.Baumschule;

import java.util.List;

public abstract class Tree {
	private int maxSize;
	private int maxDiameter;
	private FertilizationMethod fertilizationMethod;

	public Tree(int maxSize, int maxDiameter, FertilizationMethod fertilizationMethod) {
		super();
		this.maxSize = maxSize;
		this.maxDiameter = maxDiameter;
		this.fertilizationMethod = fertilizationMethod;
	}

	public FertilizationMethod getFertilizationMethod() {
		return fertilizationMethod;
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	public int getMaxDiameter() {
		return maxDiameter;
	}

	public void setMaxDiameter(int maxDiameter) {
		this.maxDiameter = maxDiameter;
	}

}
