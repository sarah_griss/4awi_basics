package at.gri.Baumschule;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Tree> trees = new ArrayList();
		GrowingArea ga = new GrowingArea("Plantage", 500, trees);
		FertilizationMethod TopGreen = new TopGreen();
		FertilizationMethod SuperGrow = new SuperGrow();
		Tree t1 = new LeafTree(100, 20, TopGreen);
		Tree t2 = new Conifer(190, 23, SuperGrow);

		ga.addTree(t1);
		trees.add(t2);

		t1.getFertilizationMethod().fertilize();

	}

}
