package at.gri.Baumschule;

public interface FertilizationMethod {
	void fertilize();
}
